# подключаем нужные модули
import pygame

# подключаем pygame
pygame.init()

# класс для карточек 
class Area():
    # конструктор, задаем начальные параметры
    # место карточки x y, её ширина и высота width и height
    # и цвет color
    def __init__(self, x=0, y=0, width=10, height=10, color=None):
        self.rect = pygame.Rect(x, y,width, height) # прямоугольная область
        self.fill_color = back # цвет карточки
        if color:
            self.fill_color = color

    # изменение цвета карточки
    def color(self, new_color):
        self.fill_color = new_color

    # закрашивание карточки цветом
    def fill(self):
        pygame.draw.rect(window, self.fill_color, self.rect)

    # проверка на нажатие на карточку
    def collidepoint(self, x, y):
        return self.rect.collidepoint(x, y)
    
    # проверка на столкновение двух спрайтов
    def colliderect(self, rect):
        return self.rect.colliderect(rect)


# класс-наследник для карточек с текстом
class Label(Area):
    # установить текст для карточки
    def set_text(self, text, fsize=12, color=(0,0,0)):
        self.font = pygame.font.SysFont('verdana', fsize) # создание шрифта с размером fsize
        self.image = self.font.render(text,True, color) # картинка с текстом цвета text_color

    # отрисовка карточки и текста
    def draw(self, shift_x=0, shift_y=0):
        self.fill() # заливаем цветом карточку
        window.blit(self.image,(self.rect.x + shift_x, self.rect.y + shift_y)) # отображаем текст со смещением вправо вниз

# класс-наследник для карточек с картинкой
class Picture(Area):
    # конструктор, задаем начальные параметры
    # место карточки x y, её ширина и высота width и height
    # и картинка filename
    def __init__(self, filename, x=0, y=0, width=10, height=10):
        super().__init__(x=x, y=y, width=width, height=height, color=None) # конструктор суперкласса Area
        self.image = pygame.image.load(filename) # картинка

    # отрисовка картинки
    def draw(self):
        window.blit(self.image, (self.rect.x, self.rect.y))

back = (200, 255, 255) # цвет фона
window = pygame.display.set_mode((500, 500)) # окно игры
window.fill(back) # закрашиваем фон
clock = pygame.time.Clock() # внутриигровые часы

# начальные координаты ракетки
racket_x = 200
racket_y = 300

# индикатор продолжения игры
game = True

# индикатор движения ракетки
move_right = False
move_left = False

# скорость ракетки
racket_speed = 6

# скорость мячика
speed_x = 4
speed_y = 4

# мячик
ball = Picture('ball.png',160, 200, 50, 50)
# ракетка
racket = Picture('platform.png',racket_x, racket_y, 100, 30)

# начальные координаты монстров (первый монстр)
start_x = 5
start_y = 5

count = 9 # количество монстров в первом ряду
monsters = [] # список монстров
# создаем три ряда монстров
for i in range(3):
    # координаты текущего монстра
    x = start_x + (27.5 * i) 
    y = start_y + (55 * i)
    for j in range(count):
        monster = Picture('enemy.png', x, y, 50, 50) # монстр
        monsters.append(monster) # добавляем его в список
        x += 55 # сдвигаемся вправо
    count -= 1 # уменьшаем количество монстров в следующем ряду на 1

""" Игровой цикл """
while game:
    # закрашиваем карточки мячика и ракетки
    ball.fill()
    racket.fill()

    # перебираем список событий
    for event in pygame.event.get():
        # если нажата кнопка "Выход"
        if event.type == pygame.QUIT:
            game = False # завершаем игру
        # ищем события "Нажатие клавиши"
        if event.type == pygame.KEYDOWN:
            # если нажата стрелка влево
            if event.key == pygame.K_LEFT:
                move_left = True # двигаемся влево
            # если нажата стрелка вправо
            if event.key == pygame.K_RIGHT:
                move_right = True # двигаемся вправо
        # ищем события "Поднятие клавиши"
        elif event.type == pygame.KEYUP:
            # если отпущена стрелка влево
            if event.key == pygame.K_LEFT:
                move_left = False # не двигаемся влево
            # если отпущена стрелка вправо
            if event.key == pygame.K_RIGHT:
                move_right = False # не двигаемся вправо

    # если двигаемся влево
    if move_left:
        racket.rect.x -= racket_speed # перемещаем ракетку влево
    # если двигаемся вправо
    if move_right:
        racket.rect.x += racket_speed # перемещаем ракетку вправо

    # перемещаем мячик по диагонали
    ball.rect.x += speed_x
    ball.rect.y += speed_y

    # если мяч ударился об верхнюю границу
    if ball.rect.y < 0:
        speed_y *= -1 # изменяем движение по вертикали на противоположное
    # если мяч ударился об левую или правую границу
    if ball.rect.x < 0 or ball.rect.x > 450:
        speed_x *= -1 # изменяем движение по горизонтали на противоположное

    # если мяч ударился об ракетку
    if ball.colliderect(racket.rect):
        speed_y *= -1 # изменяем движение по вертикали на противоположное

    # если мяч улетел ниже ракетки
    if ball.rect.y > (racket_y + 20):
        # выводим надпись о проигрыше
        game_end = Label(150, 150, 50, 50, back)
        game_end.set_text('YOU LOSE!', 60, (255, 0, 0))
        game_end.draw(10,10)
        # завершаем игровой цикл
        game = False

    # если монстров не осталось
    if len(monsters) == 0:
        # выводим надпись о победе
        game_end = Label(150, 150, 50, 50, back)
        game_end.set_text('YOU WIN!', 60, (0, 200, 0))
        game_end.draw(10,10)
        # завершаем игровой цикл
        game = False

    # отрисовываем всех монстров в цикле
    for monster in monsters:
        monster.draw()
        # если об монстра ударился мяч
        if monster.colliderect(ball.rect):
            # удаляем монстра
            monsters.remove(monster)
            monster.fill()
            # изменяем движение мяча по вертикали на противоположное
            speed_y *= -1 

    # отображаем картинку мяча и ракетки
    ball.draw()
    racket.draw()
    # обновляем все окно игры
    pygame.display.update()
    clock.tick(40)